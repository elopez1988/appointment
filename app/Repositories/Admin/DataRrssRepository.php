<?php

namespace App\Repositories\Admin;

use App\Models\Admin\DataRrss;
use App\Repositories\BaseRepository;

/**
 * Class DataRrssRepository
 * @package App\Repositories\Admin
 * @version August 1, 2019, 3:13 pm UTC
*/

class DataRrssRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'data_id',
        'rrss_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DataRrss::class;
    }
}
