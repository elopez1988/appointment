<?php

namespace App\Repositories\Admin;

use App\Models\Admin\DataCategory;
use App\Repositories\BaseRepository;

/**
 * Class DataCategoryRepository
 * @package App\Repositories\Admin
 * @version July 30, 2019, 7:43 pm UTC
*/

class DataCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'data_id',
        'category_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DataCategory::class;
    }
}
