<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Item;
use App\Repositories\BaseRepository;

/**
 * Class ItemRepository
 * @package App\Repositories\Admin
 * @version July 30, 2019, 7:43 pm UTC
*/

class ItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function search($filter)
    {
        return $this->model->search($filter);
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Item::class;
    }
}
