<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Rrss;
use App\Repositories\BaseRepository;

/**
 * Class RrssRepository
 * @package App\Repositories\Admin
 * @version August 1, 2019, 3:13 pm UTC
*/

class RrssRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function search($filter)
    {
        return $this->model->search($filter);
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rrss::class;
    }
}
