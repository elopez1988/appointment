<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Data;
use App\Repositories\BaseRepository;

/**
 * Class DataRepository
 * @package App\Repositories\Admin
 * @version July 30, 2019, 7:43 pm UTC
*/

class DataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'lastname',
        'email',
        'ig_user',
        'phone',
        'name_company',
        'img',
        'comment',
        'id_item'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Return searchable fields
     * @param array $Inpunt, $inputCategory, $inputRrss
     * @return array
     */
    public function saveDetails($input,$inputCategory,$inputRrss)
    {  
         //Crear el registro en data
        $data = $this->create($input);

        //Recorre las categorias selecionadas y la guarda en tabla pivot
        foreach ($inputCategory['categories'] as $value) {
            $data->dataCategories()->create(['category_id' => $value]); 
        }

        //Recorre las rrss selecionadas y la guarda en tabla pivot
        foreach ($inputRrss['rrss'] as $value) {
            $data->dataRrsses()->create(['rrss_id' => $value]); 
        }

        return $data;
    }


    /**
     * Return searchable fields
     *
     * @return array
     */
    public function search($filter)
    {
        return $this->model->search($filter);
    }


    /**
     * Configure the Model
     **/
    public function model()
    {
        return Data::class;
    }
}
