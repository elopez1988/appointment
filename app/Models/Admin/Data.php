<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Data",
 *      required={""},
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ig_user",
 *          description="ig_user",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="skype_user",
 *          description="skype_user",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name_company",
 *          description="name_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="img",
 *          description="img",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="comment",
 *          description="comment",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_item",
 *          description="id_item",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="enum"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="date"
 *           ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="time"
 *      ),
 *      @SWG\Property(
 *          property="categories",
 *          description="categories Array",
 *          type="{array}"
 *      )
 * )
 */
class Data extends Model
{
    use SoftDeletes;

    public $table = 'data';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'lastname',
        'email',
        'ig_user',
        'phone',
        'name_company',
        'img',
        'comment',
        'id_item',
        'skype_user',
        'status',
        'date',
        'time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'ig_user' => 'string',
        'phone' => 'string',
        'name_company' => 'string',
        'img' => 'string',
        'comment' => 'string',
        'skype_user' => 'string',
        'id_item' => 'integer',
        'status' => 'integer'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        // 'nameItem',
        'categories',
        'rrss',
        'item'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'lastname' => 'required',
        'email' => 'required',
        'ig_user' => 'required',
        'phone' => 'required',
        'name_company' => 'required',
        // 'img' => 'required',
        'comment' => 'required',
        'id_item' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Admin\Item::class, 'id_item');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     **/
    public function dataCategories()
    {
        return $this->hasMany(\App\Models\Admin\DataCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     **/
    public function dataRrsses()
    {
        return $this->hasMany(\App\Models\Admin\DataRrss::class);
    }

    /**
     * Get the name in the given Item (Accessor).
     *
     * @return string
     */
    public function getNameItemAttribute()
    {
        return $this->item()->first()->name;
    }

    /**
     * Get the name in the given Item (Accessor).
     *
     * @return string
     */
    public function getCategoriesAttribute()
    {
        return $this->dataCategories()->get();
    }

    /**
     * Get the name in the given Item (Accessor).
     *
     * @return string
     */
    public function getRrssAttribute()
    {
        return $this->dataRrsses()->get();
    }

    public function getItemAttribute() 
    {
        return $this->item()->first();
    }

    /**
     * Scope a query to find a data.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $filter)
    {
        return $query->where('name', 'like', "%$filter%")
            ->orWhere('lastname', 'like', "%$filter%")
            ->orWhere('email', 'like', "%$filter%")
            ->orWhere('ig_user', 'like', "%$filter%")
            ->orWhere('date', 'like', "%$filter%")
            ->orWhereHas('dataCategories', function($query) use ($filter) {
                $query->whereHas('category', function($query) use ($filter) {
                    $query->where('name', 'like', "%$filter%");  
                }); 
            })
            ->orWhereHas('item', function($query) use($filter) {
                $query->where('name', 'like', "%$filter%");
            });
    }
}
