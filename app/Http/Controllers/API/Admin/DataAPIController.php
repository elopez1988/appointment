<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateDataAPIRequest;
use App\Http\Requests\API\Admin\UpdateDataAPIRequest;
use App\Models\Admin\Data;
use App\User;
use App\Repositories\Admin\DataRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\DataMail;
use App\Mail\DataAdminMail;


/**
 * Class DataController
 * @package App\Http\Controllers\API\Admin
 */

class DataAPIController extends AppBaseController
{
    /** @var  DataRepository */
    private $dataRepository;

    public function __construct(DataRepository $dataRepo)
    {
        $this->dataRepository = $dataRepo;
    }

    /**
     * Metodo para convertir un string a fecha con el
     *  formato: "dd-mm-YYYY o dd/mm/YYYY"
     *
     * @param String $strDate
     * @return void
     */
    private function parseStrToDate(String $strDate) {       
        $format = strpos($strDate, "-") ? "d-m-Y" : "d/m/Y";
        try {
            $date = Carbon::createFromFormat($format, $strDate);
            return $date->toDateString();
        } catch (\Exception $e) {
            return null;            
        }
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/data",
     *      summary="Get a listing of the Data.",
     *      tags={"Data"},
     *      description="Get all Data",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Data")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $data = $this->dataRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        if ($request->has('query')) {
            $filter = $request->get('query');
            // dd($filter);
            
            if (strlen($filter) == 10) {
                $filter = $this->parseStrToDate($filter);
            }
            
            $data = $this->dataRepository->search($filter)->get();
            return $this->sendResponse($data->toArray(), 'Data search successfully');
        }
        
        return $this->sendResponse($data->toArray(), 'Data retrieved successfully');
    }

    /**
     * @param CreateDataAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/data",
     *      summary="Store a newly created Data in storage",
     *      tags={"Data"},
     *      description="Store Data",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Data that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Data")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Data"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDataAPIRequest $request)
    {
        $input         = $request->except(['categories','rrss']);
        $inputCategory = $request->only(['categories']);
        $inputRrss     = $request->only(['rrss']);
        
        // DB::beginTransaction();
        try {
            $data = $this->dataRepository->saveDetails($input,$inputCategory,$inputRrss);

            // SendMail
            $this->sendMail($data);

            // BD::commit();
        } catch (QueryException $e) {
            return response()->json(['message' => $e, 'success' => false],409);
            // DB::rollback();
        } catch (\Exception $e) {
            return response()->json(['message' => $e, 'success' => false], 409);
            // DB::rollback();
        } 
        
        return $this->sendResponse($data->toArray(), 'Data saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/data/{id}",
     *      summary="Display the specified Data",
     *      tags={"Data"},
     *      description="Get Data",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Data",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Data"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Data $data */
        $data = $this->dataRepository->find($id);

        if (empty($data)) {
            return $this->sendError('Data not found');
        }

        return $this->sendResponse($data->toArray(), 'Data retrieved successfully');
    }


    public function sendMail(Data $data)
    {
        // Email para el Cliente
        Mail::to($data->email)->send(new DataMail($data));
        // Email para el Administrador
        $user = User::find(1);
        Mail::to($user->email)->send(new DataAdminMail($data));
    }

    /**
     * @param int $id
     * @param UpdateDataAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/data/{id}",
     *      summary="Update the specified Data in storage",
     *      tags={"Data"},
     *      description="Update Data",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Data",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Data that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Data")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Data"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDataAPIRequest $request)
    {
        $input = $request->all();

        /** @var Data $data */
        $data = $this->dataRepository->find($id);

        if (empty($data)) {
            return $this->sendError('Data not found');
        }

        $data = $this->dataRepository->update($input, $id);
        // Envio de EMail
        $this->sendMail($data);

        return $this->sendResponse($data->toArray(), 'Data updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/data/{id}",
     *      summary="Remove the specified Data from storage",
     *      tags={"Data"},
     *      description="Delete Data",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Data",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Data $data */
        $data = $this->dataRepository->find($id);

        if (empty($data)) {
            return $this->sendError('Data not found');
        }

        $data->delete();

        return $this->sendResponse($id, 'Data deleted successfully');
    }

    public function pendings(Request $request) 
    {
        $data = Data::where('status', 0)->count();        
        return $this->sendResponse(["count" => $data], 'Data retrieved successfully');
    }
}
