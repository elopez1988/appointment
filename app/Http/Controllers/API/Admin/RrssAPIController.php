<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRrssAPIRequest;
use App\Http\Requests\API\Admin\UpdateRrssAPIRequest;
use App\Models\Admin\Rrss;
use App\Repositories\Admin\RrssRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RrssController
 * @package App\Http\Controllers\API\Admin
 */

class RrssAPIController extends AppBaseController
{
    /** @var  RrssRepository */
    private $rrssRepository;

    public function __construct(RrssRepository $rrssRepo)
    {
        $this->rrssRepository = $rrssRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/rrsses",
     *      summary="Get a listing of the Rrsses.",
     *      tags={"Rrss"},
     *      description="Get all Rrsses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Rrss")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $rrsses = $this->rrssRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        if ($request->has('active')) {
            return $this->sendResponse($rrsses->where('active', true)->toArray(), 'Items retrieved successfully');
        }

        if ($request->has('query')) {
            $filter = $request->get('query');
            $rrssFilter = $this->rrssRepository->search($filter)->get();
            return $this->sendResponse($rrssFilter->toArray(), 'Rrss search successfully');
        }

        return $this->sendResponse($rrsses->toArray(), 'Rrsses retrieved successfully');
    }

    /**
     * @param CreateRrssAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/rrsses",
     *      summary="Store a newly created Rrss in storage",
     *      tags={"Rrss"},
     *      description="Store Rrss",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rrss that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rrss")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rrss"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRrssAPIRequest $request)
    {
        $input = $request->all();

        $rrss = $this->rrssRepository->create($input);

        return $this->sendResponse($rrss->toArray(), 'Rrss saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/rrsses/{id}",
     *      summary="Display the specified Rrss",
     *      tags={"Rrss"},
     *      description="Get Rrss",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rrss",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rrss"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Rrss $rrss */
        $rrss = $this->rrssRepository->find($id);

        if (empty($rrss)) {
            return $this->sendError('Rrss not found');
        }

        return $this->sendResponse($rrss->toArray(), 'Rrss retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRrssAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/rrsses/{id}",
     *      summary="Update the specified Rrss in storage",
     *      tags={"Rrss"},
     *      description="Update Rrss",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rrss",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rrss that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rrss")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rrss"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRrssAPIRequest $request)
    {
        $input = $request->all();

        /** @var Rrss $rrss */
        $rrss = $this->rrssRepository->find($id);

        if (empty($rrss)) {
            return $this->sendError('Rrss not found');
        }

        $rrss = $this->rrssRepository->update($input, $id);

        return $this->sendResponse($rrss->toArray(), 'Rrss updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/rrsses/{id}",
     *      summary="Remove the specified Rrss from storage",
     *      tags={"Rrss"},
     *      description="Delete Rrss",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rrss",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Rrss $rrss */
        $rrss = $this->rrssRepository->find($id);

        if (empty($rrss)) {
            return $this->sendError('Rrss not found');
        }

        $rrss->delete();

        return $this->sendResponse($id, 'Rrss deleted successfully');
    }
}
