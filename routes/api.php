<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\AuthController@register');
Route::post('login', 'Auth\AuthController@login');

Route::get('items', 'Admin\ItemAPIController@index');
Route::get('categories', 'Admin\CategoryAPIController@index');
Route::get('rrsses', 'Admin\RrssAPIController@index');
Route::post('data', 'Admin\DataAPIController@store');

Route::get('v1/items', 'Admin\ItemAPIController@index');
Route::get('v1/rrsses', 'Admin\RrssAPIController@index');
Route::post('v1/data', 'Admin\DataAPIController@store');

Route::group(['prefix' => 'admin', 'middleware' => 'auth:api'], function () {
    Route::get('v1/categories', 'Admin\CategoryAPIController@index');
    Route::resource('categories', 'Admin\CategoryAPIController');
    Route::resource('items', 'Admin\ItemAPIController');
    Route::resource('data', 'Admin\DataAPIController');
    Route::resource('rrsses', 'Admin\RrssAPIController');
    // Citas pendientes
    Route::get('pendings', 'Admin\DataAPIController@pendings');
});





