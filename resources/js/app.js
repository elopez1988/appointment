import Vue from 'vue'

import './plugins'
import vuetify from './plugins/vuetify' // path to vuetify export

import App from './App.vue'

import router from './router'
import store from './store'

new Vue({
    vuetify,
    router,
    store,
    render: h => h(App)
}).$mount('#app')
