'use strict'

import BaseService from './base.service'

const CATEGORIES_ENDPOINT = '/api/admin/categories'

export const categoryService = new BaseService(CATEGORIES_ENDPOINT)
