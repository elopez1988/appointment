'use strict'

import BaseService from './base.service'

const RRSS_ENDPOINT = '/api/admin/rrsses'

export const socialService = new BaseService(RRSS_ENDPOINT)
