'use strict'

import BaseService from './base.service'

const APPOINTMENT_ENDPOINT = '/api/admin/data'

export const appointmentService = new BaseService(APPOINTMENT_ENDPOINT)
