'use strict'

import axios from '../plugins/axios'

export default class BaseService {
    constructor(url) {
        this.url = url
    }

    get(id, params) {
        return axios({
            url: `${this.url}/${id}`,
            method: 'get',
            params: params
        })
    }

    add(data) {
        return axios({
            url: this.url,
            method: 'post',
            data
        })
    }

    update(data) {
        return axios({
            url: `${this.url}/${data.id}`,
            method: 'put',
            data
        })
    }

    remove(id) {
        return axios({
            url: `${this.url}/${id}`,
            method: 'delete'
        })
    }

    getAll(q) {
        return axios({
            url: this.url,
            method: 'get',
            params: q
        })
    }
}
