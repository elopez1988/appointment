'use strict'

import BaseService from './base.service'

const ITEMS_ENDPOINT = '/api/admin/items'

export const itemService = new BaseService(ITEMS_ENDPOINT)
