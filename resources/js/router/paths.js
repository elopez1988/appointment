import clientRoutes from './client'
import adminRoutes from './admin'
import Login from '../admin/views/Login'

export default [
    {
        path: '*',
        redirect: { path: '/404' }
    },
    {
        path: '/404',
        meta: { public: true },
        name: 'NotFound',
        component: { template: '<h1 class="text-xs-center">Not Found</h1>' }
    },
    {
        path: '/login',
        meta: { public: true },
        name: 'Login',
        component: Login
    },
    ...clientRoutes,
    ...adminRoutes
]
