import DefaultLayout from '../layouts/admin/DefaultLayout'
import CategoriesList from '../admin/views/categories/CategoriesList'
import ItemsList from '../admin/views/items/ItemsList'
import SocialsList from '../admin/views/rrss/SocialsList'
import AppointmentList from '../admin/views/appointment/AppointmentList'
import Dashboard from '../admin/views/dashboard/Dashboard'

const adminRoutes = [
    {
        path: '/admin',
        name: 'admin',
        meta: { auth: true },
        redirect: { name: 'dashboard' },
        component: DefaultLayout,
        children: [
            {
                path: '/admin/dashboard',
                name: 'dashboard',
                meta: { auth: true },
                component: Dashboard
            },
            {
                path: '/admin/categories',
                name: 'categories',
                meta: { auth: true },
                component: CategoriesList
            },
            {
                path: '/admin/items',
                name: 'items',
                meta: { auth: true },
                component: ItemsList
            },
            {
                path: '/admin/socials',
                name: 'socials',
                meta: { auth: true },
                component: SocialsList
            },
            {
                path: '/admin/appointment',
                name: 'appointment',
                meta: { auth: true },
                component: AppointmentList
            }
        ]
    }
]

export default adminRoutes
