import DefaultLayout from '../layouts/client/DefaultLayout'
import Home from '../client/views/Home'

const clientRoutes = [
    {
        path: '/',
        name: 'client',
        redirect: { name: 'home' },
        component: DefaultLayout,
        children: [
            {
                path: '/home',
                name: 'home',
                component: Home
            }
        ]
    }
]

export default clientRoutes