import Vue from 'vue'
import Router from 'vue-router'
import paths from './paths'
import store from '../store'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: paths
})

/*
 * Protegemos las rutas  para que solo puedan acceder
 * si el usuario esta autentificado, de lo contrario
 * lo redirigimos a la ventana de Login
 */
router.beforeEach((to, from, next) => {
    const user = store.state.auth.user
    const auth = to.meta.auth

    if (auth && !user) {
        next({ name: 'Login' })
    } else {
        next()
    }

    /*
    const user = store.getters.currentUser
    const token = store.getters.token
    const auth = to.meta.auth

    // Para el manejo de Idioma en los formularios
    const language = to.params.language || 1
    store.dispatch('setFormLanguage', language)

    if (auth && (!user || !token)) {
        next({ name: 'login' })
    } else {
        NProgress.start()
        next()
    }
    */
})

router.afterEach((to, from) => {
    // NProgress.done()
})

export default router
