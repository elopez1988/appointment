import Vue from 'vue'
import axios from 'axios'
import store from '../store'
import router from '../router'

const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API,
    timeout: 5000
})

service.interceptors.request.use(
    config => {
        // Enviamos el Token en cada petición axios
        if (store.state.auth.token) {
            config.headers['Authorization'] = 'Bearer ' + store.state.auth.token
        }
        return config
    },
    error => {
        Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    response => response,
    error => {
        const { status, statusText } = error.response

        // Si no tiene un Token o está vencido, lo enviamos al formulario de Login
        if (
            status === 401 &&
            ['Unauthorized', 'InvalidToken'].indexOf(statusText) > -1
        ) {
            store.commit('auth/logout')
            router.push({ name: 'Login' })
        }
        return Promise.reject(error)
    }
)

window.axios = service

export default service
