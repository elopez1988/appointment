// Add files (nameOfPlugin.js) in '/plugins' for dependencies to integrate with project

export * from './axios'
export * from './vee-validate'
export * from './vue-sweetalert'
export * from './vue-moment'
export * from './vue2-filters'
