import Vue from 'vue'
import es from 'vee-validate/dist/locale/es'
import VeeValidate, { Validator } from 'vee-validate'

Vue.use(VeeValidate, { delay: 600 })
Validator.localize('es', es)
