import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import auth from './modules/authentication'
import data from './modules/data'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})

export default new Vuex.Store({
    state: {
        drawer: true,
        loading: false
    },
    mutations: {
        toggleDrawer: state => (state.drawer = !state.drawer),
        setDrawer(state, value) {
            state.drawer = value
        },
        setLoading(state, value) {
            state.loading = value
        }
    },
    modules: {
        auth,
        data
    },
    plugins: [vuexLocal.plugin]
})
