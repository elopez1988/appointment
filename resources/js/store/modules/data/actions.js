import axios from '../../../plugins/axios'

export async function getPendings(context) {
    try {
        const { data } = await axios.get('/api/admin/pendings')
        context.commit('setPending', data.data.count)
    } catch (e) {
        context.commit('setPending', 0)
    }
}
