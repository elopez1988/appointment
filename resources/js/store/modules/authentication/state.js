export default {
    user: null,
    token: null,
    isLogged: false,
    error: false,
    errorMessage: ''
}
