export function setUser(state, user) {
    state.user = user
    state.isLogged = true
    state.error = false
    state.errorMessage = ''
}

export function setToken(state, token) {
    state.token = token
}

export function logout(state) {
    state.user = null
    state.isLogged = false
    state.token = null
}

export function authError(state, error) {
    state.user = null
    state.token = null
    state.isLogged = false
    state.error = true
    state.errorMessage = error
}

export function clearAuthError(state) {
    state.error = false
    state.errorMessage = ''
}
