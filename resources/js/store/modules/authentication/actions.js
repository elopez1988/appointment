import axios from 'axios'

export async function signIn(context, user) {
    try {
        context.commit('setLoading', true, { root: true })

        const { data } = await axios.post('/api/login', {
            email: user.email,
            password: user.password
        })

        context.commit('setToken', data.token)
        context.commit('setUser', data.user)
    } catch (e) {
        // context.commit('authError', e.response.data.error)
        context.commit('authError', 'Usuario o contraseña invalida.')
    } finally {
        context.commit('setLoading', false, { root: true })
    }
}
