<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataRrssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_rrss', function (Blueprint $table) {
            $table->Increments('id');

            $table->unsignedInteger('data_id');
            $table->foreign('data_id')->references('id')->on('data')->onDelete('cascade');

            $table->unsignedInteger('rrss_id');
            $table->foreign('rrss_id')->references('id')->on('rrss')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_rrss');
    }
}
