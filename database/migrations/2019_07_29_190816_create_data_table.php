<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->Increments('id');

            $table->string('name');
            $table->string('lastname');
            $table->string('email');
            $table->string('ig_user');
            $table->string('phone');
            $table->string('name_company');
            $table->string('img')->nullable();
            $table->string('comment');
            $table->string('skype_user')->nullable();
            $table->integer('status')->default(0);
            $table->date('date')->nullable();
            $table->time('time')->nullable();

            $table->unsignedInteger('id_item');
            $table->foreign('id_item')->references('id')->on('items');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
