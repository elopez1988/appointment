<?php

use Illuminate\Database\Seeder;

class RrssTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rrss')->insert([
            [
                'code'      => '01email', // 1
                'name'      => 'Correo electronico',
                'active'    => true
            ],
            [
                'code'      => '02wa', // 2
                'name'      => 'Whatsapp',
                'active'    => true
            ],
            [
                'code'      => '03sk', // 3
                'name'      => 'Skype',
                'active'    => true
            ]
        ]);
    }
}
