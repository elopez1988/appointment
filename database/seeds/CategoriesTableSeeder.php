<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            [
                'code'       => '01ase', // 1
                'name'       => 'Asesoria',
                'active'     => true
            ],
            [
                'code'       => '02web', // 2
                'name'       => 'Pagina web',
                'active'     => true
            ],
            [
                'code'       => '03bra', // 3
                'name'       => 'Branding',
                'active'     => true
            ],
            [
                'code'       => '04dis', // 4
                'name'       => 'Diseño',
                'active'     => true
            ],
            [
                'code'      => '05red', // 5
                'name'      => 'Redes Sociales',
                'active'    => true
            ],
        ]);
    }
}
