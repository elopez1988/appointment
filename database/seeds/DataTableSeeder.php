<?php

use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Admin\Data::class, 10)->create()->each(function ($data) {
            $data->dataCategories()->saveMany(factory(App\Models\Admin\DataCategory::class, random_int(1, 5))->make());
        });        
    }
}
