<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('items')->insert([
            [
                'code'      => '01gas', // 1
                'name'      => 'Gastronomia',
                'active'    => true
            ],
            [
                'code'      => '02dep', // 2
                'name'      => 'Deporte',
                'active'    => true
            ],
            [
                'code'      => '03emp', // 3
                'name'      => 'Emprendimiento' ,
                'active'    => true
            ],
            [
                'code'      => '04hot', // 4
                'name'      => 'Hoteleria',
                'active'    => true
            ]
        ]);
    }
}
