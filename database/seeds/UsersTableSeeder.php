<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                'name'      => 'admin', // 1
                'email'     => 'admin@backoffice.com',
                'password'  => Hash::make('12345678')
            ],
            
        ]);
    }
}
