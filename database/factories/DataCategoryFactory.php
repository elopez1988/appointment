<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Admin\DataCategory::class, function (Faker $faker) {
    return [
        'category_id' => $faker->numberBetween(1, 5),
        'created_at' => now(),
    ];
});