<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Admin\Data::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'lastname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '1 years'),
        'time' => $faker->time,
        'phone' => $faker->phoneNumber,
        'comment' => $faker->text,
        'ig_user' => 'slevinsk21',
        'name_company' => $faker->company,
        'id_item' => $faker->numberBetween(1, 4),
        'created_at' => now(),
    ];
});